<?php
namespace api\series;

use api\lib\database\Database;
use api\lib\database\Queries;
use api\lib\Response;

class UserSeries {
	function get() {
		session_start();
		if(!isset($_SESSION['sessionId']) || $_SESSION['sessionId'] < time() - 60 * 30) {
			Header('Location: http://'.$_SERVER['SERVER_NAME'].'/project/index.php/home');
		}
		$query = Queries::getUserSeries();
		$pdo = [
			':userId' => $_SESSION['id']
		];
		$userSeries = Database::fetchAll($query, $pdo);

		Response::success('userSeries', true, '', $userSeries, [$_GET]);
	}
}