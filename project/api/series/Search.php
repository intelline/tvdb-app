<?php
namespace api\series;

use api\lib\Helpers;
use api\config\Config;
use api\lib\Response;

class Search {
	function get() {
		Helpers::setArrayDefaultValues($_GET, ['seriesName']);

		$filteredInput = Helpers::filterInput($_GET, [
				'seriesName' => 'trim'
			]
		);

		Helpers::validate($filteredInput, [
				'seriesName' => 'required|max_len,100'
			]
		);

		$url = Config::$baseApiUrl.'GetSeries.php?seriesname='.$filteredInput['seriesName'];
		$series = simplexml_load_file($url);

		$series = Helpers::objectToArray($series);
		
		Response::success('search', true, '', $series['Series'], $_GET);
	}
}