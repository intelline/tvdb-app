<?php
namespace api\session;

use api\lib\Response;

class Logout {
	function post() {
		session_start();
		if(!isset($_SESSION['sessionId'])) {
			return;
		}

		session_destroy();
		
		Response::success('logout', true, '', [], $_POST);
	}
}