<?php
namespace api\session;

use api\lib\database\Database;
use api\lib\database\Queries;
use api\lib\Helpers;
use api\lib\Response;
use Exception;

class Login {
	function post() {
		session_start();
		if(isset($_SESSION['sessionId']) && $_SESSION['sessionId'] >= time() - 60 * 30) {
			return;
		}

		Helpers::setArrayDefaultValues($_POST, ['email', 'password']);

		$filteredInput = Helpers::filterInput($_POST, [
				'email'		=> 'trim',
				'password'	=> 'trim'
			]
		);

		Helpers::validate($filteredInput, [
				'email'		=> 'required|valid_email',
				'password'	=> 'required|max_len,100|min_len,6',
			]
		);

		$email    = $filteredInput['email'];
		$password = $filteredInput['password'];
		
		$query = Queries::getUser();

		$pdo = [
			':email'	=> $email,
			':password' => md5($password)
		];

		$userInfo = Database::fetch($query, $pdo);

		if(!$userInfo) {
			throw new Exception('Invalid username or password.');
		}

		$_SESSION['id']			= $userInfo['id'];
		$_SESSION['username']	= $userInfo['username'];
		$_SESSION['sessionId']	= time();

		Response::success('login', true, '', $userInfo, $_POST);
	}	
}