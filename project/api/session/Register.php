<?php
namespace api\session;

use api\lib\database\Database;
use api\lib\database\Queries;
use api\lib\Helpers;
use api\lib\Response;
use Exception;

class Register {
	function post() {
		session_start();
		if(isset($_SESSION['sessionId']) && $_SESSION['sessionId'] >= time() - 60 * 30) {
			return;
		}

		$allUsers   = Database::fetchAll(Queries::getAllUsers());
		$usernames	= array_column($allUsers, 'username');
		$emails		= array_column($allUsers, 'email');

		Helpers::setArrayDefaultValues($_POST, ['username', 'email', 'password']);

		$filteredInput = Helpers::filterInput($_POST, [
				'username'	=> 'trim',
				'email'		=> 'trim',
				'password'	=> 'trim'
			]
		);

		Helpers::validate($filteredInput, [
				'username'	=> 'required|alpha_numeric|max_len,100|min_len,6|doesNotcontainList,'.implode(';', $usernames),
				'email'		=> 'required|valid_email|doesNotcontainList,'.implode(';', $emails),
				'password'	=> 'required|max_len,100|min_len,6',
			]
		);

		$username = $filteredInput['username'];
		$email    = $filteredInput['email'];
		$password = $filteredInput['password'];
		
		$query = Queries::insertUser();

		$pdo = [
			':username' => $username,
			':email'	=> $email,
			':password' => md5($password)
		];

		$stmt = Database::getConnection()->prepare($query);
		$stmt->execute($pdo);

		Response::success('register', true, '', [], $_POST);
	}
}