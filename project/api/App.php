<?php
namespace api;

use api\lib\vendor\router\Toro;
use api\lib\vendor\router\ToroHook;
use Exception;

class App {
	public function __construct() {
		$this->match();
	}

	private function match() {
		ToroHook::add("404", function() {
			throw new Exception('Not found');
		});
		Toro::serve([
			'/login'		=> 'api\session\Login',
			'/logout'		=> 'api\session\Logout',
			'/register'		=> 'api\session\Register',
			'/search'		=> 'api\series\Search',
			'/myseries'		=> 'api\series\UserSeries',
			'/home'			=> 'api\session\Home'
		]);
	}
}