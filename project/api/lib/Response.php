<?php
namespace api\lib;

class Response {
	public static function success($action='', $success=true, $error='', array $data, array $params) {
		return json_encode([
			'action'	=> $action,
			'success'	=> $success,
			'error'		=> $error,
			'data'		=> $data,
			'params'	=> $params,
		]);
	}
}
