<?php
namespace api\lib\database;

class Queries {
	// or username = username
	public static function getUser() {
		return 'SELECT `id`, `username`, `email`, `password` 
				FROM `users`
				WHERE 1 AND `email`=:email AND `password`=:password#';
	}

	public static function getAllUsers() {
		return 'SELECT `username`, `email`, `password` 
				FROM `users`
				WHERE 1';
	}

	public static function insertUser() {
		return 'INSERT INTO `users`(`username`, `email`, `password`) 
				VALUES (:username,:email,:password)';
	}

	public static function getUserSeries() {
		return 'SELECT `series` 
				FROM `users_series`
				WHERE 1 AND `userId`=:userId';
	}
}