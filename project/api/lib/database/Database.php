<?php
namespace api\lib\database;

use api\config\Config;
use PDO;

class Database {
	private static $connection;

	private function __constuct() {}

	public static function getConnection() {
		if(isset(self::$connection)) {
			return self::$connection;
		}
		$configDb = Config::$db;
		self::$connection = new PDO('mysql:host='.$configDb['host'].';dbname='.$configDb['database'], $configDb['username'], $configDb['password']);
		return self::$connection;
	}

	public static function fetch($query, $pdo = []) {
		$stmt = self::getConnection()->prepare($query);
		$stmt->execute($pdo);
		return $stmt->fetch(PDO::FETCH_ASSOC);
	}

	public static function fetchAll($query, $pdo = []) {
		$stmt = self::getConnection()->prepare($query);
		$stmt->execute($pdo);
		return $stmt->fetchAll(PDO::FETCH_ASSOC);
	}

	public static function fetchColumn($query, $pdo = [], $columns = 1) {
		$stmt = self::getConnection()->prepare($query);
		$stmt->execute($pdo);
		return $stmt->fetchColumn($columns);
	}
}