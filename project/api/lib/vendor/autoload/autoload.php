<?php
namespace lib\vender\autoload;

require_once 'SplClassLoader.php';
use lib\vendor\autoload\SplClassLoader;

$loader = new SplClassLoader();
$loader->register();