<?php
namespace api\lib;

use api\lib\vendor\validation\GUMP;
use Exception;

class Helpers {
	public static function validate(array $input, array $rules) {
		$is_valid = GUMP::is_valid($input, $rules);

		if($is_valid !== true) {
			throw new Exception($is_valid[0]);
		}
	}

	public static function filterInput(array $input, array $rules) {
		$gump = new GUMP();
		$input = $gump->sanitize($input);
		return $gump->filter($input, $rules);
	}

	public static function objectToArray($object) {
		if(!is_object($object) && !is_array($object)) {
			return $object;
		}
		return array_map('self::objectToArray', (array) $object);
	}

	public static function setArrayDefaultValues(array &$array, array $keys) {
		foreach ($keys as $key) {
			if(!isset($array[$key])) {
				$array[$key] = '';
			}
		}
	}
}