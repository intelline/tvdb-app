<?php
namespace api\config;

class Config {
	public static $db = [
		'host'		=> 'localhost',
		'database'	=> 'ivan_project',
		'username'	=> 'root',
		'password'	=> ''
	];

	public static $apiKey = '52F6D118BA73B2B7';

	public static $baseApiUrl = 'http://thetvdb.com/api/';
}