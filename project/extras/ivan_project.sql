-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 
-- Версия на сървъра: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ivan_project`
--

-- --------------------------------------------------------

--
-- Структура на таблица `episodes`
--

CREATE TABLE IF NOT EXISTS `episodes` (
  `id` int(11) NOT NULL,
  `series` varchar(255) NOT NULL,
  `season` int(11) NOT NULL,
  `episode` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура на таблица `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;

--
-- Схема на данните от таблица `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`) VALUES
(1, 'ivanadamov', 'ivanadamov@abv.bg', 'dba48a4205fcbc3007a932c47fdd99f8'),
(2, 'ivanadamova', 'ivanadamov@abv.bga', 'dba48a4205fcbc3007a932c47fdd99f8'),
(3, 'ivanadamovaa', 'ivanadamov@abv.bgaa', 'dba48a4205fcbc3007a932c47fdd99f8'),
(4, 'ivanadamovaaa', 'ivanadamov@abv.bgaaa', 'dba48a4205fcbc3007a932c47fdd99f8'),
(36, 'ivanadamovaaaa', 'ivanadamov@abv.bgaaaa', 'dba48a4205fcbc3007a932c47fdd99f8'),
(37, 'ivanadamovaaaaa', 'ivanadamov@abv.bgs', 'dba48a4205fcbc3007a932c47fdd99f8'),
(38, 'ivanadamovaaaaaa', 'ivanadamov@abv.bgss', 'dba48a4205fcbc3007a932c47fdd99f8'),
(39, 'ivanadamovaaaf', 'ivanadamov@abv.bgaaas', 'dba48a4205fcbc3007a932c47fdd99f8');

-- --------------------------------------------------------

--
-- Структура на таблица `users_series`
--

CREATE TABLE IF NOT EXISTS `users_series` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `series` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Схема на данните от таблица `users_series`
--

INSERT INTO `users_series` (`id`, `userId`, `series`) VALUES
(1, 1, 'manamana'),
(2, 1, 'yabadabadu'),
(3, 1, 'manamana'),
(4, 1, 'yabadabadu');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `episodes`
--
ALTER TABLE `episodes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `username` (`username`), ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `users_series`
--
ALTER TABLE `users_series`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `episodes`
--
ALTER TABLE `episodes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `users_series`
--
ALTER TABLE `users_series`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
