<?php
use api\lib\Response;

class TestResponse extends PHPUnit_Framework_TestCase {
	
	public function testSuccess() {
		$expected = '{"action":"aaa","success":true,"error":"","data":[],"params":[]}';
		$actual= Response::success('aaa', true, '', [], []);
		
		$this->assertEquals(1, 1);
		$this->assertEquals($expected, $actual);
	}
}